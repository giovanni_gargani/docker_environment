# Ambiente di sviluppo dockerizzato.

Questo repository contiene una configurazione iniziale per avere un ambiente di sviluppo completamente dockerizzato. Di default vengono lanciati:

 1.  un db postgres
 2.  un db mysql
 3.  un'istanza redis
 4.  un'istanza di adminer per amministrare i database (adminer.localhost)
 5.  un'istanza di rebrow per ispezionare redis (rebrow.localhost)
 6.  traefik per gestire il tutto (traefik.localhost:8080)

 Non c'è un server apache perché nel mio environment la versione dockerizzata risultava tremendamente inefficiente, inoltre impostare xdebug era un incubo. Ci sono traccie di configurazione, ma le levero'

 Io mi trovo bene (su windows) con [uWamp](https://www.uwamp.com/en/) ma imagino che anche [Xampp](https://www.apachefriends.org/it/download.html)  va benissimo.
 Ovviamente uwamp si lamenterà che non puo' lanciare mysql (perché già avviato). A me sta bene cosi', ma se preferite usare il mysql di uwamp basta commentare il service mysql in docker-compose o configurare .env perché non venga mai restartato (e quindi una volta che lo fermate dopo il primo lancio non parte piu')



